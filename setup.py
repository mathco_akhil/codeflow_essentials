from setuptools import setup

setup(name='codeflow_essentials',
      version='0.1',
      description='Reusable modules for common modelling techniques',
      long_description='Utility modules that help accelerate data science model development process. Can be integrated into existing Python code',
      classifiers=[
            'Development Status :: 0 - Dev',
            'License :: OSI Approved :: MIT License',
            'Programming Language :: Python :: 3.7.1',
      ],
      url='https://gitlab.com/mathco_akhil/codeflow_essentials.git',
      author='MathCo Data Science teams',
      author_email='akhil@themathcompany.com',
      license='MIT',
      packages=['forecast'],
      install_requires = ['pandas','numpy'],
      include_package_data=True,
      zip_safe=False)
