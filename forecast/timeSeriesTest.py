from statsmodels.tsa.stattools import adfuller
from statsmodels.stats.diagnostic import acorr_ljungbox
import pandas as pd
import numpy as np

def do_adf(obj, maxlag=None, regression="c", autolag='AIC', store=False, regresults=False):
    null_hypothesis = None 
    adfstat, pvalue, usedlag, nobs, critvalues, icbest = adfuller(obj, maxlag=None, regression="c", autolag='AIC', store=False, regresults=False)

    if pvalue <= 0.05:
        null_hypothesis = 'Accepted'
    else:
        null_hypothesis = 'Rejected'

    return {
        'adf_test_statistic': adfstat,
        'p-value': pvalue,
        'lags_used': usedlag,
        'n_obs': nobs,
        'critical values': critvalues,
        'best_ic': icbest,
        'null_hypothesis': null_hypothesis
    }

def check_stationarity(series):
    testResults = do_adf(series)
    if testResults['null_hypothesis'] == 'Rejected':
        return True
    else:
        return False

def are_residuals_autocorr(series, n_lags =1, boxpierce=False):
    lbvalue,pvalue = acorr_ljungbox(series, lags=n_lags, boxpierce=boxpierce)
    k=0
    for i in range(len(pvalue)):
        if pvalue[i]<0.05:
            k+=1
    if k>len(series)/5:
        print("residuals is auto correlated")
        return True
    else:
        return False