import pandas as pd
import numpy as np
import warnings
warnings.simplefilter("ignore")

def seperate_sku_series(df, dep_var):
    sku = []
    for i in df.material_update.unique():
        series = df[df['material_update'] == i].resample('M').mean()[dep_var]
        sku.append((series,i))
    return sku

def treat_missing_values(sku):
    for i in sku:
         i[0].fillna(np.median(i[0]), inplace = True)
    return sku

def train_test_split(series):
    dfTrain =series[:-12]
    dfTest = series[-12:]
    return dfTrain,dfTest