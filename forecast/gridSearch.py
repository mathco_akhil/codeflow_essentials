from statsmodels.tsa.arima_model import ARIMA
import pandas as pd
import numpy as np
import statsmodels.api as sm
from forecast import timeSeriesTest
from forecast import modelEvaluation

def get_p(series):
    pacf_series = sm.tsa.stattools.pacf(series.diff(), nlags=int(len(series.diff())*2/3))
    for i in range(1, len(pacf_series)):
        sig_var = np.sqrt(len(series)-i)
        if pacf_series[i]< (1.96/sig_var) and pacf_series[i]> (-1.96/sig_var):
            return i
    return 1

def get_d(series):
    d = 0
    if timeSeriesTest.check_stationarity(series) == False:
        d=1
    return d

def get_q(series):
    acf_series = sm.tsa.stattools.acf(series.diff(), nlags=int(len(series.diff())*2/3))
    for i in range(1,len(acf_series)):
        sig_var = np.sqrt(len(series)-i)
        if acf_series[i]< (1.96/sig_var) and acf_series[i]> (-1.96/sig_var):
            return i
    return 1

def build_model(series,p,d,q):
    transparams=True
    if p<2:
        p=2
    if q<1:
        q=1
    if q>=p:
        q=p-1
    try:
        best_model = ARIMA(series, order = (p,d,q)).fit()
        curr_mape = modelEvaluation.cal_mape(best_model.resid,series)
    except Exception as e:
        print(e)
        return "unable to prepare arima with initial order " + str((p,d,q))
    opt_mape = np.inf
    while(curr_mape<opt_mape):
        opt_mape=curr_mape
        mape=[]
        try:
            model1 = ARIMA(series, order = (p+1,d,q)).fit(transparams=transparams, disp=-1)
            mape.append(modelEvaluation.cal_mape(best_model.resid,series))
        except Exception as e:
            print("Error occured at order "+str((p+1,d,q)))
            print(e)
        try:
            if q<p:
                model2 = ARIMA(series, order = (p,d,q+1)).fit(transparams=transparams, disp=-1)
                mape.append(modelEvaluation.cal_mape(best_model.resid,series))
        except Exception as e:
            print("Error occured at order "+str((p,d,q+1)))
            print(e)
        if p>1 and q<p-1:
            try:
                model3 = ARIMA(series, order = (p-1,d,q)).fit(transparams=transparams, disp=-1)
                mape.append(modelEvaluation.cal_mape(best_model.resid,series))
            except Exception as e:
                print("Error occured at order "+str((p-1,d,q)))
                print(e)
        if q>1:
            try:
                model4 = ARIMA(series, order = (p,d,q-1)).fit(transparams=transparams, disp=-1)
                mape.append(modelEvaluation.cal_mape(best_model.resid,series))
            except Exception as e:
                print("Error occured at order "+str((p,d,q-1)))
                print(e)                
        i = mape.index(min(mape))
        if i==1:
            p=p+1
        elif i==2:
            q=q+1
        elif i==3:
            p=p-1
        elif i==4:
            q=q-1
        curr_mape = mape[i]
        if curr_mape == opt_mape:
            break
    try:
        best_model = ARIMA(series, order = (p,d,q)).fit(transparmas=transparams, disp=-1)
        best_mape = modelEvaluation.cal_mape(best_model.resid,series)
        return (best_model,best_mape, (p,d,q))
    except Exception as e:
        print(e) 
        return "Unable to built best model"