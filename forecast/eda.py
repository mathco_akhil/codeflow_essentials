import statsmodels.api as sm
from statsmodels.tsa.stattools import acf, pacf
import pandas as pd
import numpy as np
import warnings
warnings.simplefilter("ignore")
from plotly.offline import iplot,init_notebook_mode
import plotly.graph_objs as go
init_notebook_mode(connected=True)

def generateHistogram(series,dep_var,nbins = None):
    data = [go.Histogram(x=series,autobinx=False,nbinsx=nbins,visible=True,name=str(dep_var),marker=dict(color='rgb(0,31,95)'))]
    layout = go.Layout(
            title=("Histogram of "+dep_var).title(),
                     xaxis=dict(
                             linewidth = 2,
                             title=(dep_var+' range').lower(),
                             zeroline=True,
                             showline=True,
                             titlefont=dict(
                                 family='Courier New, monospace',
                                size=18,
                                 color='#7f7f7f'
                             )
                        ),
                    yaxis=dict(
                            linewidth = 2,
                            title=('Number of values in '+dep_var+' bins').lower(),
                            zeroline=True,
                            showline=True,
                            titlefont=dict(
                                family='Courier New, monospace',
                                size=18,
                                color='#7f7f7f'
                            )
                        )
                )

    fig=go.Figure(data=data, layout=layout)
    return fig

def stl(series, dep_var, model='additive', filt=None, freq=None, two_sided=True, extrapolate_trend=0):
    # decomposing data into trend, sesonal and irregular component
    stlDecompose = sm.tsa.seasonal_decompose(series, model=model, filt=filt, freq=freq, two_sided=two_sided, extrapolate_trend=extrapolate_trend)
    return stlDecompose.trend, stlDecompose.seasonal, stlDecompose.resid

def plot_stl(trend, seasonal, resid):
    # setting diffrent component of data into figure
    data = [go.Scatter(x=trend.index,y=trend)]
    layout = go.Layout(
                title="Trend",
                yaxis=dict(
                    title="Trend"
                ),
                xaxis=dict(
                    title="Timeline"
                )
            )
    trendPlot = go.Figure(data=data,layout=layout)
    data = [go.Scatter(x=seasonal.index,y=seasonal)]
    layout = go.Layout(
                title="Seasonality",
                yaxis=dict(
                    title="Seasonality"
                ),
                xaxis=dict(
                    title="Timeline"
                )
            )
    seasonalPlot = go.Figure(data=data,layout=layout)
    data = [go.Scatter(x=resid.index,y=resid)]
    layout = go.Layout(
                title="Irregularity",
                yaxis=dict(
                    title="Irregularity"
                ),
                xaxis=dict(
                    title="Timeline"
                )
            )
    irregularPlot = go.Figure(data=data,layout=layout)
    return trendPlot,seasonalPlot,irregularPlot

def plot_acf_pacf(series,dep_var,n_lags=1):
    acf_values = acf(series, nlags=n_lags)
    sig_pos = [0]
    sig_neg = [0]
    for i in range(1,len(series)):
        sig_var = np.sqrt(len(series))
        sig_pos.append(1.96/sig_var)
        sig_neg.append(-1.96/sig_var)
    
    sig_range_pos = go.Scatter(x=np.array(range(n_lags+1)), y=sig_pos, mode='lines')
    sig_range_neg = go.Scatter(x=np.array(range(n_lags+1)), y=sig_neg, mode='lines')
    
    trace_acf = go.Bar(x=np.array(range(n_lags+1)), y=acf_values)
    data = [trace_acf, sig_range_pos, sig_range_neg]    
    layout = go.Layout(
        title = "ACF Plot",
        yaxis2=dict(
            title= ("% of event").lower(),
            titlefont=dict(
                color='rgb(0, 0, 0)'
            ),
            tickfont=dict(
                color='rgb(0, 0, 0)'
            ),
            overlaying='y',
            side='right',
            zeroline=False,
            showline=True
        )
    )
    acf_fig = {'data': data, 'layout': layout}

    pacf_values = pacf(series, nlags=n_lags)
    trace_pacf = go.Bar(x=np.array(range(n_lags+1)), y=pacf_values)
    
    sig_range_pos = go.Scatter(x=np.array(range(n_lags+1)), y=sig_pos, mode='lines')
    sig_range_neg = go.Scatter(x=np.array(range(n_lags+1)), y=sig_neg, mode='lines')
    
    data = [trace_pacf, sig_range_pos, sig_range_neg]  
    layout = go.Layout(
        title = "PACF Plot",
    )
    pacf_fig = {'data': data, 'layout': layout}

    return acf_fig, pacf_fig

