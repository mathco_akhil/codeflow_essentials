import pandas as pd
import numpy as np

def cal_mape(resid,series):
    mape= np.mean(np.abs(resid/series)) * 100
    return mape

def get_aic(model):
    return model.aic
